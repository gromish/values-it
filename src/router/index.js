// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: "/valutazioni",
    component: () => import('@/layouts/default/Root.vue'),
  }, 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})


export default router
