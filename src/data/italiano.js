
const italiano = 
    {
        // materia
        label: "Italiano",
            // prove: ,
            // annotazioni: 1,
        nuclei: [
            {
                label:"Ascoltare e comprendere",

                classi: [
                    {
                    label:"1",
                    valutazioni: [
                        "L'alunno ascolta e comprende in modo completo e approfondito la totalità dei messaggi trasmessi.",
                        "L'alunno ascolta e comprende in modo prevalentemente corretto.",                        
                        "L'alunno ascolta e comprende in modo frammentario i messaggi orali. ",                        
                        "L'alunno ascolta e comprende le informazioni principali del messaggio trasmesso se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"2",
                    valutazioni: [
                        "L'alunno ascolta e comprende in modo completo e approfondito la totalità dei messaggi trasmessi.",
                        "L'alunno ascolta e comprende in modo prevalentemente corretto.",
                        "L'alunno ascolta e comprende in modo frammentario i messaggi orali.",
                        "L'alunno ascolta e comprende le informazioni principali del messaggio trasmesso se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"3",
                    valutazioni: [
                        "L'alunno ascolta e comprende in modo completo e approfondito la totalità dei messaggi trasmessi.",
                        "L'alunno ascolta e comprende in modo prevalentemente corretto.",
                        "L'alunno ascolta e comprende in modo frammentario i messaggi orali.",
                        "L'alunno ascolta e comprende le informazioni principali del messaggio trasmesso se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"4",
                    valutazioni: [
                        "L'alunno ascolta e comprende in modo completo e approfondito la totalità dei messaggi trasmessi. ",
                        "L'alunno ascolta e comprende in modo prevalentemente corretto.",
                        "L'alunno ascolta e comprende in modo frammentario i messaggi orali.",
                        "L'alunno ascolta e comprende le informazioni principali del messaggio trasmesso se guidato dall'insegnante.                        ",
                        ]
                    },
                    {
                    label:"5",
                    valutazioni: [
                        "L'alunno ascolta e comprende in modo completo e approfondito la totalità dei messaggi trasmessi.",
                        "L'alunno ascolta e comprende in modo prevalentemente corretto.",
                        "L'alunno ascolta e comprende in modo frammentario i messaggi orali.",
                        "L'alunno ascolta e comprende le informazioni principali del messaggio trasmesso se guidato dall'insegnante.",
                        ]
                    },
                ]
            },
            {
                label:"Parlare",

                classi: [
                    {
                    label:"1",
                    valutazioni: [
                        "L'alunno interagisce e racconta in modo corretto e chiaro. ",
                        "L'alunno interagisce e racconta in modo prevalentemente corretto e chiaro.",
                        "L'alunno interagisce e racconta in modo parzialmente corretto. ",
                        "L'alunno interagisce e racconta se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"2",
                    valutazioni: [
                        "L'alunno interagisce e racconta in modo corretto e chiaro. ",
                        "L'alunnointeragisce e racconta in modo prevalentemente corretto e chiaro.",
                        "L'alunno interagisce e racconta in modo parzialmente corretto. ",
                        "L'alunno interagisce e racconta se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"3",
                    valutazioni: [
                        "L'alunno interagisce e racconta in modo pertinente ed articolato.",
                        "L'alunno interagisce e racconta in modo prevalentemente  pertinente e corretto.",
                        "L'alunno interagisce e racconta in modo parzialmente corretto e pertinente.",
                        "L'alunno interagisce e racconta se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"4",
                    valutazioni: [
                        "L'alunno interagisce e racconta in modo pertinente, fluido ed articolato.",
                        "L'alunno interagisce e racconta in modo prevalentemente pertinente e corretto.",
                        "L'alunno interagisce e racconta in modo parzialmente corretto e pertinente.",
                        "L'alunno interagisce e racconta se guidato dall'insegnante.",
                        ]
                    },
                    {
                    label:"5",
                    valutazioni: [
                        "L'alunno interagisce e racconta in modo pertinente, fluido ed articolato.",
                        "L'alunno interagisce e racconta in modo prevalentemente pertinente e corretto.",
                        "L'alunno interagisce e racconta in modo parzialmente corretto e pertinente.",
                        "L'alunno interagisce e racconta se guidato dall'insegnante.",
                        ]
                    },
                ]
            },
            {
                label:"Leggere e comprendere",

                classi: [
                    {
                    label:"1",
                    valutazioni: [
                        ]
                    },
                    {
                    label:"2",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"3",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"4",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"5",
                    valutazioni: [

                        ]
                    },
                ]
            },
            {
                label:"Scrivere",

                classi: [
                    {
                    label:"1",
                    valutazioni: [
                        ]
                    },
                    {
                    label:"2",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"3",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"4",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"5",
                    valutazioni: [

                        ]
                    },
                ]
            },
            {
                label:"Riflettere",

                classi: [
                    {
                    label:"1",
                    valutazioni: [
                        ]
                    },
                    {
                    label:"2",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"3",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"4",
                    valutazioni: [

                        ]
                    },
                    {
                    label:"5",
                    valutazioni: [

                        ]
                    },
                ]
            },
        ]
    }

export default italiano