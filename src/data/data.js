import italiano from "@/data/italiano.js";
import francese from "@/data/francese.js";
import arte from "@/data/arte.js";
import civica from "@/data/civica.js";
import geografia from "@/data/geografia.js";
import inglese from "@/data/inglese.js";
import matematica from "@/data/matematica.js";
import motoria from "@/data/motoria.js";
import musica from "@/data/musica.js";
import scienze from "@/data/scienze.js";
import storia from "@/data/storia.js";
import tecnologia from "@/data/tecnologia.js";


const data = [
    italiano,
    francese,
    arte,
    civica,
    geografia,
    inglese,
    matematica,
    motoria,
    musica,
    scienze,
    storia,
    tecnologia,
]


export default data